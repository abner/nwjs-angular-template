function MainController() {
    var vm = this;

    vm.version = '0.0.1';

    vm.openDevTools = function () {
        window.NwGui.Window.get().showDevTools();
    };

    vm.reload = function () {
        window.NwGui.Window.get().reloadDev();
    };

    vm.quit = function () {
        window.NwGui.App.closeAllWindows();
    }
}

module.exports = MainController;