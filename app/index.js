

var document = window.document;
var angular = require('angular');

window.NwGui = window.nwDispatcher.requireNwGui();

//require('bootstrap-loader');

(function (angular) {

    var app = angular.module('myApp', []);
    
    var mainController = require('./main/mainController.js');
    
    app.controller('MainController', mainController);

    angular.element(document).ready(function () {
        angular.bootstrap(document, ['myApp']);
    });


})(angular);


