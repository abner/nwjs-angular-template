/* global __dirname */

var path = require('path');
var webpack = require('webpack');

var Fs = require('fs')
var nodeModules = {}
Fs.readdirSync('node_modules').forEach(function (module) {
    if (module !== '.bin') nodeModules[module] = 'commonjs ' + module
})

module.exports = {
    entry: {
        'bootstrap': 'bootstrap-loader',
        'app': './index.js'
    },
    context: __dirname + '/app',
    /*externals: nodeModules,*/
    output:
    {
        path: path.join(__dirname, "js"),
        filename: "[name].bundle.js",
        chunkFilename: "[id].chunk.js"
    },
    /*
    devtool: options.devtool,
    debug: options.debug,
    */
    module: {
        loaders: [
            // Bootstrap 3
            { test: /bootstrap-sass\/assets\/javascripts\//, loader: 'imports?jQuery=jquery' },
            /*{ test: /\.(woff2?|svg)$/, loader: 'url?limit=10000' },
            { test: /\.(ttf|eot)$/, loader: 'file' },*/
             {
                test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                loader: 'url-loader'
            }
        ],
        noParse: [/\.min\.js/]
    },
    resolve: {
        extensions: [
            '',
            '.js',
            '.json'
        ],
        modulesDirectories: [
            './node_modules'
        ]
    },
    plugins: [
         new webpack.NoErrorsPlugin(),
        new webpack.optimize.CommonsChunkPlugin({
            name: "commons",
            filename: "commons.js",
        })
    ]
};
